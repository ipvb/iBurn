//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by iBurn.rc
//

#define IDS_APP_TITLE			103
#define IDS_APP_USEINFO   117

#define IDR_MAINFRAME			128
#define IDD_IBURN_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_IBURN			107
#define IDI_SMALL				108
#define IDC_IBURN			109
#define IDI_ICD                110
#define IDI_IBD                 111
#define IDI_IDVD              112
#define IDI_IHD                113
#define IDI_IBURNH          114
#define IDI_ISHARE           115
#define IDI_IUSBALT          116



#define IDC_COMBOX_DEVICE    201
#define IDC_EDIT_IMAGE             202
#define IDC_PROCESS_TIME        203
#define IDC_BUTTON_VIEW         204
#define IDC_BUTTON_BOOTFIX   205
#define IDC_BUTTON_MAKE        206
#define IDC_STATIC_SIZE             207


#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// 新对象的下一组默认值
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
