// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
// Windows 头文件:
#include <windows.h>
#include <windowsx.h>

// C 运行时头文件
#include <stdio.h>
#include <stdlib.h>
#include <new>
#include <wchar.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <dwmapi.h>
#include <CommCtrl.h>
#include <Uxtheme.h>
#include <strsafe.h>
#include <ShlObj.h>
#include <Shlwapi.h>

#pragma comment (lib ,"dwmapi.lib")
#pragma comment (lib, "UxTheme.lib")
#pragma comment (lib,"comctl32.lib")


// TODO: 在此处引用程序需要的其他头文件
//Progress bar ---进程状态条 msctls_progress32
//利用Button 放大设置边框。
//ComboBoxEx32 WC_COMBOBOXEX
//ComboBox Style:CS_VREDRAW