/***/
#include "stdafx.h"
#include "ISOFormat.h"

int ExtractToUsbDisk(HWND hWnd,LPWSTR ISOFileName,LPWSTR DriveChar)
{
	//7z
	//GetPath
	TCHAR szPath[260]={0};
	TCHAR _7z_Path[512]={L'\0'};
	GetModuleFileName(NULL,szPath,260);
	PROCESS_INFORMATION pi;
	STARTUPINFO sInfo;
	DWORD dwExitCode;
	ZeroMemory(&sInfo,sizeof(sInfo));
	sInfo.cb=sizeof(sInfo);
	sInfo.dwFlags=STARTF_USESHOWWINDOW;
	sInfo.wShowWindow=SW_HIDE;
	ZeroMemory(&pi,sizeof(pi));
	(wcsrchr(szPath,_T('\\')))[1]=0;
	//(wcsrchr(szPath,_T('\\')))[1]=0;
	wcscat_s(_7z_Path,szPath);
	wcscat_s(_7z_Path,L"..\\7-Zip\\7z.exe");
	wcscat_s(_7z_Path,L" x ");
	wcscat_s(_7z_Path,ISOFileName);
	wcscat_s(_7z_Path,L" -o");
	wcscat_s(_7z_Path,DriveChar);
	//MessageBox(NULL,_7z_Path,L"OK",MB_OK);
	// 7z x ISOFileName -oDriveChar
	DWORD result=CreateProcess(NULL,_7z_Path,NULL,NULL,NULL,CREATE_NO_WINDOW,NULL,NULL,&sInfo,&pi);
	if(result==TRUE)
	{
		CloseHandle(pi.hThread);
		WaitForSingleObject(pi.hProcess,INFINITE);
		GetExitCodeProcess(pi.hProcess,&dwExitCode);
		CloseHandle(pi.hProcess);
		if(dwExitCode!=0)
		{
			MessageBox(hWnd,L"Extract the ISO file failed !",L"Can't Write",MB_OK|MB_ICONHAND);
			return -1;
		}
	}
	return 0;
}