#ifndef USBFIND_H
#define USBFIND_H
typedef unsigned int uInt;

//typedef struct UsbDevice
//{
//	uInt UsbDeviceS;
//	wchar_t  String[26][50];
//}

typedef struct _UsbDevice
{
	uInt Id;
	wchar_t deviceLetter[10];
	wchar_t RootPath[20];
	wchar_t Info[260];
}UsbDevice;
uInt FindUSBDisk();

extern UsbDevice DefUsbDevice[26];

#endif