#include "stdafx.h"
#include <shellapi.h>

bool FormatDrive(HWND hWnd,LPWSTR DeviceLetter)
{
	//Format Drive
	//format.com
	//diskpart
	//format DriveLetter /FS:NTFS /v:UsbBootDisk  /q /x /A:4096 /y
	TCHAR FormatCmd[260]={L"\0"};
	wcscpy_s(FormatCmd,L"format ");
	wcscat_s(FormatCmd,DeviceLetter);
	wcscat_s(FormatCmd,L" /FS:NTFS /v:UsbInstall /q /x /A:4096 /y");
	PROCESS_INFORMATION pi;
	STARTUPINFO sInfo;
	DWORD dwExitCode;
	ZeroMemory(&sInfo,sizeof(sInfo));
	sInfo.cb=sizeof(sInfo);
	sInfo.dwFlags=STARTF_USESHOWWINDOW;
	sInfo.wShowWindow=SW_HIDE;
	ZeroMemory(&pi,sizeof(pi));
	//GetModuleFileName(NULL,szPath,260);
	DWORD result=CreateProcess(NULL,FormatCmd,NULL,NULL,NULL,CREATE_NO_WINDOW,NULL,NULL,&sInfo,&pi);
	if(result==TRUE)
	{
		CloseHandle(pi.hThread);
		WaitForSingleObject(pi.hProcess,INFINITE);
		GetExitCodeProcess(pi.hProcess,&dwExitCode);
		CloseHandle(pi.hProcess);
		if(dwExitCode!=0)
		{
			MessageBox(hWnd,L"Your Disk Maybe have a Error ,It can't Format this !",L"Format Error",MB_OK|MB_ICONHAND);
			return false;
		}
	}

	//TCHAR szPath[260]={0};
	//TCHAR BCDPath[260]={L'\0'};
	//GetModuleFileName(NULL,szPath,260);
	//(wcsrchr(szPath,_T('\\')))[1]=0;
	////(wcsrchr(szPath,_T('\\')))[1]=0;
	//wcscat_s(BCDPath,szPath);
	//wcscat_s(BCDPath,L"..\\BCDBoot\\iBurn.exe");
	//ShellExecute(hWnd,L"open",BCDPath,NULL,NULL,SW_SHOW);
	return true;
}


bool UsbBootFix(HWND hWnd,LPWSTR DeviceLetter)
{
	TCHAR szPath[260]={0};
	TCHAR BCDPath[260]={L'\0'};
	PROCESS_INFORMATION pi;
	STARTUPINFO sInfo;
	DWORD dwExitCode;
	ZeroMemory(&sInfo,sizeof(sInfo));
	sInfo.cb=sizeof(sInfo);
	sInfo.dwFlags=STARTF_USESHOWWINDOW;
	sInfo.wShowWindow=SW_HIDE;
	ZeroMemory(&pi,sizeof(pi));
	GetModuleFileName(NULL,szPath,260);
	(wcsrchr(szPath,_T('\\')))[1]=0;
	//(wcsrchr(szPath,_T('\\')))[1]=0;
	wcscat_s(BCDPath,szPath);
	wcscat_s(BCDPath,L"..\\BCDBoot\\bootsect.exe");
	wcscat_s(BCDPath,L" /NT60 ");
	wcscat_s(BCDPath,DeviceLetter);
	//ShellExecut
	DWORD result=CreateProcess(NULL,BCDPath,NULL,NULL,NULL,CREATE_NO_WINDOW,NULL,NULL,&sInfo,&pi);
	if(result==TRUE)
	{
		CloseHandle(pi.hThread);
		WaitForSingleObject(pi.hProcess,INFINITE);
		GetExitCodeProcess(pi.hProcess,&dwExitCode);
		CloseHandle(pi.hProcess);
		if(dwExitCode!=0)
		{
			MessageBox(hWnd,L"Your Disk Maybe have a Error ,It can't Boot Fix this !",L"Boot Fix Error",MB_OK|MB_ICONHAND);
			return false;
		}
	}
	//ShellExecute(hWnd,L"open",BCDPath,NULL,NULL,SW_HIDE);
	//WaitForSingleObject
	//ShellBrowserWindow
	//CreatePocess
	//bootsect /NT60 DriveLetter /force 
	return true;
}