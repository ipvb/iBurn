#pragma once

#include "resource.h"

LRESULT OpenImageFile(HWND hWnd,PWSTR Imagename);
bool UsbBootFix(HWND hWnd,LPWSTR DriveLetter);
bool FormatDrive(HWND hWnd,LPWSTR DriveLetter);
