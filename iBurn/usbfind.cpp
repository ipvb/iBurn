#include "stdafx.h"
#include "usbfind.h"
UsbDevice DefUsbDevice[26];
uInt  FindUSBDisk()
{
	UINT xdw = 0;
	TCHAR Buffer[2048] = { 0 };
	TCHAR drivePath[9] = { L"\0" };
	TCHAR driveLetter[5] = { L"\0" };
	//dw = GetLogicalDrives();
	DWORD iSub = 0;
	DWORD iLength= GetLogicalDriveStrings(2048, Buffer);
	for (iSub = 0; iSub < iLength; iSub += 4)
	{
		wcscpy_s(drivePath, Buffer+iSub);
		if (GetDriveType(drivePath) == DRIVE_REMOVABLE)
		{
			if (GetVolumeInformation(drivePath, 0, 0, 0, 0, 0, 0, 0))
			{
				
				wcscpy_s(driveLetter, drivePath);
				(wcsrchr(driveLetter, _T('\\')))[1] = 0;
				ULARGE_INTEGER lpFreeToCaller;
				ULARGE_INTEGER lpTotalSize;
				ULARGE_INTEGER lpFreeSize;
				TCHAR SizeInfoTo[70] = { L"\0" };
				TCHAR SizeInfoFree[70] = { L"\0" };
				if (GetDiskFreeSpaceEx(drivePath, &lpFreeToCaller, &lpTotalSize, &lpFreeSize) != 0)
				{

					swprintf_s(SizeInfoTo, L"  U盘总共大小: %4.1f GB\n", (float)(lpTotalSize.QuadPart) / (1024 * 1024 * 1024));
					swprintf_s(SizeInfoFree, L"  剩余容量: %4.1f GB\n", (float)(lpFreeSize.QuadPart) / (1024 * 1024 * 1024));
					wcscpy_s(DefUsbDevice[xdw].deviceLetter, driveLetter);
					wcscpy_s(DefUsbDevice[xdw].Info, drivePath);
					wcscat_s(DefUsbDevice[xdw].Info, SizeInfoTo);
					wcscat_s(DefUsbDevice[xdw].Info, SizeInfoFree);
				}
				wcscpy_s(DefUsbDevice[xdw].RootPath, drivePath);
				DefUsbDevice[xdw].Id = xdw;
				xdw++;

			}
		}
	}
	return xdw;
}